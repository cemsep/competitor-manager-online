﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CompetitorManagerOnline.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CompetitorManagerOnline.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly CompetitorManagerOnlineDBContext _context;

        public HomeController(ILogger<HomeController> logger, CompetitorManagerOnlineDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(Competitor competitor)
        {
            if (ModelState.IsValid)
            {
                _context.Competitors.Add(competitor);

                await _context.SaveChangesAsync();

                return View("AddCompetitorConfirmation", competitor);
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> AllCompetitorsAsync()
        {
            return View(await _context.Competitors.ToListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitor = await _context.Competitors.FindAsync(id);

            if (competitor == null) 
            {
                return NotFound();
            }

            return View(competitor);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCompetitor(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var competitorToUpdate = await _context.Competitors.FirstOrDefaultAsync(c => c.Id == id);
            if (await TryUpdateModelAsync<Competitor>(
                competitorToUpdate,
                "",
                c => c.FirstName, c => c.LastName, c => c.Nationality, c => c.Score, c => c.TotalStroke))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException /* ex */)
                {
                    ModelState.AddModelError("", "Unable to save changes.");
                }
            }

            return View(competitorToUpdate);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false) 
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitor = await _context.Competitors.AsNoTracking().FirstOrDefaultAsync(c => c.Id == id);

            if (competitor == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] = "Delete failed.";
            }

            return View(competitor);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var competitor = await _context.Competitors.FindAsync(id);

            if (competitor == null)
            {
                return NotFound();
            }

            try
            {
                _context.Competitors.Remove(competitor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException /* ex */)
            {
                return RedirectToAction(nameof(Delete), new { id = id, saveChangesError = true });
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
