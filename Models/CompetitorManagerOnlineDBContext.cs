﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CompetitorManagerOnline.Models
{
    public class CompetitorManagerOnlineDBContext : DbContext
    {
        public CompetitorManagerOnlineDBContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Competitor> Competitors { get; set; }
    }
}
