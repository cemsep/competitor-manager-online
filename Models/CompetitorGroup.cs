﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitorManagerOnline.Models
{
    public class CompetitorGroup
    {
        private static List<Competitor> CurrentCompetitors = new List<Competitor>();

        public static List<Competitor> Competitors
        {
            get { return CurrentCompetitors; }
        }

        public static void AddCompetitor(Competitor newCompetitor)
        {
            CurrentCompetitors.Add(newCompetitor);
        }
    }
}
