﻿using CompetitorManagerOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitorManagerOnline.Data
{
    public static class Seeder
    {
        public static void Seed(CompetitorManagerOnlineDBContext context)
        {
            using (context)
            {
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Tiger",
                    LastName = "Woods",
                    Nationality = "USA",
                    Score = -6,
                    TotalStroke = 68
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Jason",
                    LastName = "Day",
                    Nationality = "AUS",
                    Score = -7,
                    TotalStroke = 67
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Ian",
                    LastName = "Poulter",
                    Nationality = "ENG",
                    Score = -5,
                    TotalStroke = 71
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Jon",
                    LastName = "Rahm",
                    Nationality = "ESP",
                    Score = -5,
                    TotalStroke = 70
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Adam",
                    LastName = "Scott",
                    Nationality = "AUS",
                    Score = -7,
                    TotalStroke = 71
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Brooks",
                    LastName = "Koepka",
                    Nationality = "USA",
                    Score = -7,
                    TotalStroke = 71
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Dustin",
                    LastName = "Johnson",
                    Nationality = "USA",
                    Score = -6,
                    TotalStroke = 70
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Louis",
                    LastName = "Oosthuizen",
                    Nationality = "RSA",
                    Score = -7,
                    TotalStroke = 66
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Francesco",
                    LastName = "Molinari",
                    Nationality = "ITA",
                    Score = -7,
                    TotalStroke = 67
                });

                context.SaveChanges();
            }

        }
    }
}
