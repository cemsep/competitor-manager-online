# Competitor Manager Online

## Description

Application Type: ASP.NET Core Web Application (MVC) Allows a user to manage information about competitors.

## Contributors

Cem Pedersen     @cemsep     https://gitlab.com/cemsep
